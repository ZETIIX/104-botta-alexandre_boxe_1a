 --

-- Database: botta_alexandre_boxe_1a

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS botta_alexandre_boxe_1a;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS botta_alexandre_boxe_1a;

-- Utilisation de cette base de donnée

USE botta_alexandre_boxe_1a;


-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 10 Mars 2021 à 07:15
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `botta_alexandre_boxe_1a`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_boxer`
--

CREATE TABLE `t_boxer` (
  `idBoxer` int(11) NOT NULL,
  `fkPersonne` int(11) NOT NULL,
  `tailleBoxer` int(11) NOT NULL,
  `poidsBoxer` int(11) NOT NULL,
  `positionBoxer` varchar(45) NOT NULL,
  `allongeBoxer` int(11) NOT NULL,
  `aliasBoxer` varchar(45) NOT NULL,
  `photoBoxer` varchar(45) NOT NULL,
  `dateBoxer` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_boxer_a_combat_resultat`
--

CREATE TABLE `t_boxer_a_combat_resultat` (
  `idBoxerACombatResult` int(11) NOT NULL,
  `fkBoxer` int(11) NOT NULL,
  `fkTypeResult` int(11) NOT NULL,
  `fkCombat` int(11) NOT NULL,
  `pointsBoxerACombatResult` int(11) NOT NULL,
  `roundBoxerACombatResult` int(11) NOT NULL,
  `minuteBoxerACombatResult` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_boxer_a_recompense`
--

CREATE TABLE `t_boxer_a_recompense` (
  `idBoxerARecompense` int(11) NOT NULL,
  `fkBoxer` int(11) NOT NULL,
  `fkRecompense` int(11) NOT NULL,
  `dateAqusitionRecompense` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_combat`
--

CREATE TABLE `t_combat` (
  `idCombat` int(11) NOT NULL,
  `nbrRoundCombat` int(11) NOT NULL,
  `minuteRoundCombat` int(11) NOT NULL,
  `dateCombat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_combat_a_division`
--

CREATE TABLE `t_combat_a_division` (
  `idCombatADivision` int(11) NOT NULL,
  `fkCombat` int(11) NOT NULL,
  `fkDivision` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_combat_photo`
--

CREATE TABLE `t_combat_photo` (
  `idCombatPhoto` int(11) NOT NULL,
  `photoCombatPhoto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_combat_photo_video`
--

CREATE TABLE `t_combat_photo_video` (
  `idCombatPV` int(11) NOT NULL,
  `fkCombat` int(11) NOT NULL,
  `fkCombatPhoto` int(11) NOT NULL,
  `fkCombatVideo` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_combat_video`
--

CREATE TABLE `t_combat_video` (
  `idCombatVideo` int(11) NOT NULL,
  `videoCombatVideo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_division`
--

CREATE TABLE `t_division` (
  `idDivision` int(11) NOT NULL,
  `categorieDivision` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_nationalite`
--

CREATE TABLE `t_nationalite` (
  `idNationalite` int(11) NOT NULL,
  `nationaliteNati` text NOT NULL,
  `lieuNaissanceNati` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `idPersonne` int(11) NOT NULL,
  `nomPers` varchar(49) NOT NULL,
  `prenomPers` varchar(21) NOT NULL,
  `dateNaissPers` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_a_nationalite`
--

CREATE TABLE `t_pers_a_nationalite` (
  `idPersANationalite` int(11) NOT NULL,
  `fkPersonne` int(11) NOT NULL,
  `fkNationalite` int(11) NOT NULL,
  `dateAcquisitionNati` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_etre_typep`
--

CREATE TABLE `t_pers_etre_typep` (
  `idPersEtreTypeP` int(11) NOT NULL,
  `fkPersonne` int(11) NOT NULL,
  `fkTypeP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_recompense`
--

CREATE TABLE `t_recompense` (
  `idRecompense` int(11) NOT NULL,
  `titreRecompense` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_type_personne`
--

CREATE TABLE `t_type_personne` (
  `idTypePersonne` int(11) NOT NULL,
  `boxerTypeP` tinyint(1) NOT NULL,
  `coachTypeP` tinyint(1) NOT NULL,
  `arbitreTypeP` tinyint(1) NOT NULL,
  `managerTypeP` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_type_resultat`
--

CREATE TABLE `t_type_resultat` (
  `idTypeResultat` int(11) NOT NULL,
  `koTypeResult` tinyint(1) NOT NULL,
  `tkoTypeResult` tinyint(1) NOT NULL,
  `decisionUnanimeTypeResult` tinyint(1) NOT NULL,
  `decisionPartageeTypeResult` tinyint(1) NOT NULL,
  `egaliteTypeResult` tinyint(1) NOT NULL,
  `defaiteTypeResult` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_boxer`
--
ALTER TABLE `t_boxer`
  ADD PRIMARY KEY (`idBoxer`),
  ADD KEY `fkPersonne` (`fkPersonne`);

--
-- Index pour la table `t_boxer_a_combat_resultat`
--
ALTER TABLE `t_boxer_a_combat_resultat`
  ADD PRIMARY KEY (`idBoxerACombatResult`),
  ADD KEY `idBoxer` (`fkBoxer`),
  ADD KEY `idTypeResult` (`fkTypeResult`),
  ADD KEY `idCombat` (`fkCombat`);

--
-- Index pour la table `t_boxer_a_recompense`
--
ALTER TABLE `t_boxer_a_recompense`
  ADD PRIMARY KEY (`idBoxerARecompense`),
  ADD KEY `fkBoxer` (`fkBoxer`),
  ADD KEY `fkRecompense` (`fkRecompense`);

--
-- Index pour la table `t_combat`
--
ALTER TABLE `t_combat`
  ADD PRIMARY KEY (`idCombat`);

--
-- Index pour la table `t_combat_a_division`
--
ALTER TABLE `t_combat_a_division`
  ADD PRIMARY KEY (`idCombatADivision`),
  ADD KEY `fkCombat` (`fkCombat`),
  ADD KEY `fkDivision` (`fkDivision`);

--
-- Index pour la table `t_combat_photo`
--
ALTER TABLE `t_combat_photo`
  ADD PRIMARY KEY (`idCombatPhoto`);

--
-- Index pour la table `t_combat_photo_video`
--
ALTER TABLE `t_combat_photo_video`
  ADD PRIMARY KEY (`idCombatPV`),
  ADD KEY `fkCombat` (`fkCombat`),
  ADD KEY `fkCombatPhoto` (`fkCombatPhoto`),
  ADD KEY `fkCombatVideo` (`fkCombatVideo`);

--
-- Index pour la table `t_combat_video`
--
ALTER TABLE `t_combat_video`
  ADD PRIMARY KEY (`idCombatVideo`);

--
-- Index pour la table `t_division`
--
ALTER TABLE `t_division`
  ADD PRIMARY KEY (`idDivision`);

--
-- Index pour la table `t_nationalite`
--
ALTER TABLE `t_nationalite`
  ADD PRIMARY KEY (`idNationalite`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`idPersonne`);

--
-- Index pour la table `t_pers_a_nationalite`
--
ALTER TABLE `t_pers_a_nationalite`
  ADD PRIMARY KEY (`idPersANationalite`),
  ADD KEY `fkPersonne` (`fkPersonne`),
  ADD KEY `fkNationalite` (`fkNationalite`);

--
-- Index pour la table `t_pers_etre_typep`
--
ALTER TABLE `t_pers_etre_typep`
  ADD PRIMARY KEY (`idPersEtreTypeP`),
  ADD KEY `fkPersonne` (`fkPersonne`),
  ADD KEY `fkTypeP` (`fkTypeP`);

--
-- Index pour la table `t_recompense`
--
ALTER TABLE `t_recompense`
  ADD PRIMARY KEY (`idRecompense`);

--
-- Index pour la table `t_type_personne`
--
ALTER TABLE `t_type_personne`
  ADD PRIMARY KEY (`idTypePersonne`);

--
-- Index pour la table `t_type_resultat`
--
ALTER TABLE `t_type_resultat`
  ADD PRIMARY KEY (`idTypeResultat`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_boxer`
--
ALTER TABLE `t_boxer`
  MODIFY `idBoxer` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_boxer_a_combat_resultat`
--
ALTER TABLE `t_boxer_a_combat_resultat`
  MODIFY `idBoxerACombatResult` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_boxer_a_recompense`
--
ALTER TABLE `t_boxer_a_recompense`
  MODIFY `idBoxerARecompense` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_combat`
--
ALTER TABLE `t_combat`
  MODIFY `idCombat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_combat_a_division`
--
ALTER TABLE `t_combat_a_division`
  MODIFY `idCombatADivision` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_combat_photo`
--
ALTER TABLE `t_combat_photo`
  MODIFY `idCombatPhoto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_combat_photo_video`
--
ALTER TABLE `t_combat_photo_video`
  MODIFY `idCombatPV` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_combat_video`
--
ALTER TABLE `t_combat_video`
  MODIFY `idCombatVideo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_division`
--
ALTER TABLE `t_division`
  MODIFY `idDivision` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_nationalite`
--
ALTER TABLE `t_nationalite`
  MODIFY `idNationalite` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `idPersonne` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_a_nationalite`
--
ALTER TABLE `t_pers_a_nationalite`
  MODIFY `idPersANationalite` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_etre_typep`
--
ALTER TABLE `t_pers_etre_typep`
  MODIFY `idPersEtreTypeP` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_recompense`
--
ALTER TABLE `t_recompense`
  MODIFY `idRecompense` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_type_personne`
--
ALTER TABLE `t_type_personne`
  MODIFY `idTypePersonne` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_type_resultat`
--
ALTER TABLE `t_type_resultat`
  MODIFY `idTypeResultat` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_boxer`
--
ALTER TABLE `t_boxer`
  ADD CONSTRAINT `t_boxer_ibfk_1` FOREIGN KEY (`fkPersonne`) REFERENCES `t_personne` (`idPersonne`);

--
-- Contraintes pour la table `t_boxer_a_combat_resultat`
--
ALTER TABLE `t_boxer_a_combat_resultat`
  ADD CONSTRAINT `t_boxer_a_combat_resultat_ibfk_1` FOREIGN KEY (`fkBoxer`) REFERENCES `t_boxer` (`idBoxer`),
  ADD CONSTRAINT `t_boxer_a_combat_resultat_ibfk_2` FOREIGN KEY (`fkTypeResult`) REFERENCES `t_type_resultat` (`idTypeResultat`),
  ADD CONSTRAINT `t_boxer_a_combat_resultat_ibfk_3` FOREIGN KEY (`fkCombat`) REFERENCES `t_combat` (`idCombat`);

--
-- Contraintes pour la table `t_boxer_a_recompense`
--
ALTER TABLE `t_boxer_a_recompense`
  ADD CONSTRAINT `t_boxer_a_recompense_ibfk_1` FOREIGN KEY (`fkBoxer`) REFERENCES `t_boxer` (`idBoxer`),
  ADD CONSTRAINT `t_boxer_a_recompense_ibfk_2` FOREIGN KEY (`fkRecompense`) REFERENCES `t_recompense` (`idRecompense`);

--
-- Contraintes pour la table `t_combat_a_division`
--
ALTER TABLE `t_combat_a_division`
  ADD CONSTRAINT `t_combat_a_division_ibfk_1` FOREIGN KEY (`fkCombat`) REFERENCES `t_combat` (`idCombat`),
  ADD CONSTRAINT `t_combat_a_division_ibfk_2` FOREIGN KEY (`fkDivision`) REFERENCES `t_division` (`idDivision`);

--
-- Contraintes pour la table `t_pers_a_nationalite`
--
ALTER TABLE `t_pers_a_nationalite`
  ADD CONSTRAINT `t_pers_a_nationalite_ibfk_1` FOREIGN KEY (`fkPersonne`) REFERENCES `t_personne` (`idPersonne`),
  ADD CONSTRAINT `t_pers_a_nationalite_ibfk_2` FOREIGN KEY (`fkNationalite`) REFERENCES `t_nationalite` (`idNationalite`);

--
-- Contraintes pour la table `t_pers_etre_typep`
--
ALTER TABLE `t_pers_etre_typep`
  ADD CONSTRAINT `t_pers_etre_typep_ibfk_1` FOREIGN KEY (`fkPersonne`) REFERENCES `t_personne` (`idPersonne`),
  ADD CONSTRAINT `t_pers_etre_typep_ibfk_2` FOREIGN KEY (`fkTypeP`) REFERENCES `t_type_personne` (`idTypePersonne`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
